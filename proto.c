/*
 * proto.c
 *
 *  Created on: 21 сент. 2018 г.
 *      Author: svetozar
 */

#include "proto.h"
#include "checksum.h"

void proto_raw_write_header(UInt16_t size, UInt8_t * buf, Size_t buf_size)
{
  SDR_ASSERT(buf_size >= SDR_PROTO_HEADER_BUF_SIZE);
  int i = 0;
  buf[i++] = SDR_PROTO_START_SYM;

  buf[i++] = size & 0xff;
  buf[i++] = (size >> 8) & 0xff;

  UInt16_t crc = crc_16((unsigned char *)buf, i);
  buf[i++] = crc & 0xff;
  buf[i++] = (crc >> 8) & 0xff;
}

Bool_t proto_raw_read_header(UInt8_t * buf, Size_t buf_size, UInt16_t * size, Bool_t * crcOk)
{
  SDR_ASSERT(buf_size >= SDR_PROTO_HEADER_BUF_SIZE);
  int i = 0;
  if(buf[i++] != SDR_PROTO_START_SYM)
    return false;

  *size = buf[i++];
  *size |= (((UInt16_t)buf[i++]) << 8) & 0xff00;

  UInt16_t crc = crc_16((unsigned char *)buf, i);
  UInt16_t rxCrc = buf[i++];
  rxCrc |= (((UInt16_t)buf[i++]) << 8) & 0xff00;
  if (crcOk)
    *crcOk = crc == rxCrc;
  return true;
}

static void ProtoReader_setMode(ProtoReader_t * This, Bool_t isContent)
{
  This->isSyncLost = false;
  This->isContent = isContent;
  This->buf_counter = 0;
  CLEAR_BUFFER(UInt8_t,This->buf.P,This->buf.Size);
}


void init_ProtoReader(ProtoReader_t * This, ProtoReaderCfg_t * cfg)
{
  This->buf = cfg->buf;

  ProtoReader_setMode(This, false);

  This->Parent = 0;
  init_ProtoReaderCallback(&This->cbk);
}

void proto_reader(ProtoReader_t * This, UInt8_t byte)
{
  SDR_ASSERT(This->buf_counter < This->buf.Size);
  if(This->isContent)
  {
    if (This->buf_counter < This->content_len)
      This->buf.P[This->buf_counter++] = byte;
    else
    {
      if (byte != SDR_PROTO_FINISH_SYM)
      {
        if (This->cbk.on_warning)
          This->cbk.on_warning(This->Parent,proto_reader_wrn_FinishSymInvalid);
      }
      else if (This->cbk.on_content_ready)
        This->cbk.on_content_ready(This->Parent, (UInt8_t *)This->buf.P, This->content_len);
      ProtoReader_setMode(This,false);
    }

    if (This->buf_counter >= SDR_PROTO_HEADER_BUF_SIZE)
    {
      int idx = This->buf_counter-SDR_PROTO_HEADER_BUF_SIZE;
      if ((This->buf.P[idx] == SDR_PROTO_START_SYM))
      {
        Bool_t crcOk = false;
        UInt16_t content_len;
        Bool_t res = proto_raw_read_header(This->buf.P+idx, SDR_PROTO_HEADER_BUF_SIZE, &content_len, &crcOk);
        if (res && crcOk)
        {
          This->content_len = content_len;
          ProtoReader_setMode(This,true);
          if (This->cbk.on_warning)
            This->cbk.on_warning(This->Parent,proto_reader_wrn_ResetForHeader);
          return;
        }
      }
    }
  }
  else
  {
    if ((This->buf_counter == 0)&&(byte != SDR_PROTO_START_SYM))
    {
      if ((!This->isSyncLost))
      {
        This->isSyncLost = true;
        if (This->cbk.on_warning)
          This->cbk.on_warning(This->Parent,proto_reader_wrn_SyncLost);
      }
      return;
    }

    SDR_ASSERT(This->buf_counter < SDR_PROTO_HEADER_BUF_SIZE);
    This->buf.P[This->buf_counter++] = byte;
    if (This->buf_counter == SDR_PROTO_HEADER_BUF_SIZE)
    {
      Bool_t crcOk = false;
      Bool_t res = proto_raw_read_header(This->buf.P, This->buf_counter, &This->content_len, &crcOk);
      if (res && crcOk)
        ProtoReader_setMode(This,true);
      else
      {
        This->buf_counter = 0;
        if (This->cbk.on_warning)
          This->cbk.on_warning(This->Parent,proto_reader_wrn_crcForHeader);

        bool isNewStart = false;
        for (UInt16_t i = 1; i < SDR_PROTO_HEADER_BUF_SIZE; ++i)
        {
          if (This->buf.P[i] == SDR_PROTO_START_SYM)
            isNewStart = true;
          if (isNewStart)
            This->buf.P[This->buf_counter++] = This->buf.P[i];
        }
      }
    }
  }
}
