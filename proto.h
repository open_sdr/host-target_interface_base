/*
 * proto.h
 *
 *  Created on: 21 сент. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_INTERFACE_BASE_PROTO_H_
#define SDR_INTERFACE_BASE_PROTO_H_

#include <SDR/BASE/Abstract.h>
#include <SDR/BASE/common.h>

#define SDR_PROTO_START_SYM (0xf0u)
#define SDR_PROTO_FINISH_SYM (0x0fu)
#define SDR_PROTO_HEADER_BUF_SIZE 5

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
  byteBuffer_t buf;
} ProtoReaderCfg_t;

INLINE void init_ProtoReaderCfg(ProtoReaderCfg_t * cfg)
{
  init_byteBuffer(&cfg->buf);
}

typedef enum
{
  proto_reader_wrn_Null = 0,

  proto_reader_wrn_Generic,

  proto_reader_wrn_FinishSymInvalid,
  proto_reader_wrn_SyncLost,
  proto_reader_wrn_ResetForHeader,

  proto_reader_wrn_crcForHeader
} ProtoReaderWarning_t;

typedef void (*proto_reader_warning_fxn_t)(void * This, ProtoReaderWarning_t id);
typedef struct
{
  bytes_sink_t on_content_ready;
  proto_reader_warning_fxn_t on_warning;
} ProtoReaderCallback_t;
INLINE void init_ProtoReaderCallback(ProtoReaderCallback_t * cbk){INIT_STRUCT(ProtoReaderCallback_t,cbk);}

typedef struct
{
  Bool_t isContent, isSyncLost;
  UInt16_t content_len;

  byteBuffer_t buf;
  UInt16_t buf_counter;

  void * Parent;
  ProtoReaderCallback_t cbk;
} ProtoReader_t;

void init_ProtoReader(ProtoReader_t * This, ProtoReaderCfg_t * cfg);
INLINE void ProtoReader_setCallback(ProtoReader_t * This, void * Parent, ProtoReaderCallback_t * cbk)
{
  This->Parent = Parent;
  This->cbk = *cbk;
}

void proto_reader(ProtoReader_t * This, UInt8_t byte);

void proto_raw_write_header(UInt16_t size, UInt8_t * buf, Size_t buf_size);
Bool_t proto_raw_read_header(UInt8_t * buf, Size_t buf_size, UInt16_t * size, Bool_t * crcOk);

#ifdef __cplusplus
}
#endif

#endif /* SDR_INTERFACE_BASE_PROTO_H_ */
